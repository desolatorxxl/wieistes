package main

import (
	_ "embed"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"strings"
)

//go:embed words.txt
var words string

//go:embed index.html
var index string

func main() {
	wordArray := strings.Split(words, "\n")

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		reload := "60"
		word := strings.Title(wordArray[rand.Intn(len(wordArray))])

		w.Header().Add("Cache-Control", "no-cache")
		w.WriteHeader(http.StatusMovedPermanently)

		fmt.Fprintf(w, index, reload, word)
	})

	fmt.Printf("Starting server at port 8080\n")
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal(err)
	}
}
