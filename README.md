# wieistes

This website uses Go to provide a Web-Overlay for OBS.\
This is mainly used by a_Syucide on the Krawall e.V. Discord Server (v2 | 2021-06-18).


A local file (words.txt) is opened, loading all the files into an Array.\
After the basic site is setup, a random int is used to get a word from the aformentioned array.

The site refreshes after 60 seconds.

A target output looks like this:

```
Wie ist es? Nautisch!
```
or this:

```
Wie ist es? Gerädert!
```
&nbsp;
&nbsp;

**Warning!**\
The webserver runs on an unencrypted http connection on port 80.

&nbsp;
&nbsp;
&nbsp;
&nbsp;

Due to our laziness, this project is updated on every master commit and automatically deployed to prod.\
_Yes, really._
